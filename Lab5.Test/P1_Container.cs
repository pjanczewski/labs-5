﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MExpectedException = Microsoft.VisualStudio.TestTools.UnitTesting.ExpectedExceptionAttribute;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;
using ExpectedException = NUnit.Framework.ExpectedExceptionAttribute;
using PK.Container;
using Lab5.Infrastructure;
using Lab5.Test.Fakes;
using System.Reflection;
using PK.Test;

namespace Lab5.Test
{
    [TestFixture]
    [TestClass]
    public class P1_Container
    {
        [Test]
        [TestMethod]
        public void P1__Container_Should_Implement_IContainer()
        {
            Helpers.Should_Implement_Interface(LabDescriptor.Container, typeof(IContainer));     
        }

        [Test]
        [TestMethod]
        public void P1__Container_Should_Return_Null_For_Not_Registered_Interface()
        {
            // Arrange
            var container = (IContainer)Activator.CreateInstance(LabDescriptor.Container);

            // Act
            var result = container.Resolve<IFake>();

            // Assert
            Assert.That(result, Is.Null);
        }

        [Test]
        [TestMethod]
        public void P1__Container_Should_Register_Singleton_As_Object()
        {
            // Arrange
            var container = (IContainer)Activator.CreateInstance(LabDescriptor.Container);
            var singleton = new FakeImpl();

            // Act
            container.Register(singleton);
            var result = container.Resolve<IFake>();

            // Assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result, Is.SameAs(singleton));
        }

        [Test]
        [TestMethod]
        public void P1__Container_Should_Register_Singleton_As_Interface()
        {
            // Arrange
            var container = (IContainer)Activator.CreateInstance(LabDescriptor.Container);
            IFake singleton = new FakeImpl();

            // Act
            container.Register(singleton);
            var result = container.Resolve<IFake>();

            // Assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result, Is.SameAs(singleton));
        }

        [Test]
        [TestMethod]
        public void P1__Container_Should_Register_Prototype_Class()
        {
            // Arrange
            var container = (IContainer)Activator.CreateInstance(LabDescriptor.Container);
            
            // Act
            container.Register(typeof(FakeImpl));
            var result1 = container.Resolve<IFake>();
            var result2 = container.Resolve<IFake>();

            // Assert
            Assert.That(result1, Is.Not.Null);
            Assert.That(result2, Is.Not.Null);
            Assert.That(result1, Is.Not.SameAs(result2));
            Assert.That(result1, Is.InstanceOf<FakeImpl>());
            Assert.That(result2, Is.InstanceOf<FakeImpl>());
        }

        [Test]
        [TestMethod]
        public void P1__Container_Should_Register_Object_Provider()
        {
            // Arrange
            var container = (IContainer)Activator.CreateInstance(LabDescriptor.Container);

            // Act
            container.Register(() => new FakeImpl());
            var result = container.Resolve<IFake>();

            // Assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result, Is.InstanceOf<FakeImpl>());
        }

        [Test]
        [TestMethod]
        public void P1__Container_Should_Register_Interface_Provider()
        {
            // Arrange
            var container = (IContainer)Activator.CreateInstance(LabDescriptor.Container);

            // Act
            container.Register(() => (IFake)new FakeImpl());
            var result = container.Resolve<IFake>();

            // Assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result, Is.InstanceOf<FakeImpl>());
        }

        [Test]
        [TestMethod]
        public void P1__Container_Should_Register_All_Public_Interface_Implementations_In_Assembly()
        {
            // Arrange
            var container = (IContainer)Activator.CreateInstance(LabDescriptor.Container);

            // Act
            container.Register(Assembly.GetAssembly(typeof(IFake)));
            var result = container.Resolve<IFake>();

            // Assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result, Is.InstanceOf<PublicFakeImpl>());
        }

        [Test]
        [TestMethod]
        [MExpectedException(typeof(UnresolvedDependenciesException))]
        [ExpectedException(typeof(UnresolvedDependenciesException))]
        public void P1__Container_Should_Throw_Exception_While_Resolving_Class_Without__Resolved_Dependencies()
        {
            // Arrange
            var container = (IContainer)Activator.CreateInstance(LabDescriptor.Container);

            // Act
            container.Register(typeof(DependingFakeImpl));
            var result = container.Resolve<IDependingFake>();
        }

        [Test]
        [TestMethod]
        public void P1__Container_Should_Resolve_Dependencies()
        {
            // Arrange
            var container = (IContainer)Activator.CreateInstance(LabDescriptor.Container);

            // Act
            container.Register(typeof(DependingFakeImpl));
            container.Register(typeof(FakeImpl));
            var result = container.Resolve<IDependingFake>();

            // Assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result, Is.InstanceOf<DependingFakeImpl>());
            Assert.That(result.Fake, Is.Not.Null);
            Assert.That(result.Fake, Is.InstanceOf<FakeImpl>());
        }
    }
}

